# Pasar El repositorio de github a local
### Primero de todo para pasarlo en local con el comando $git clone "URL del repositorio"
![Primera Screenshot](https://gitlab.com/Ramago1715/git_project/-/blob/main/git1.png)

# Editar los archivos del repositorio en local

### Despues en local editamos los archivos, por ejemplo el readme (que es lo que estoy haciendo ahora)
![Screentshot edicion readme](https://gitlab.com/Ramago1715/git_project/-/blob/main/edicionreadme.png)
### Ademas de subir las caputras de pantalla
![Segunda Screenshot](https://gitlab.com/Ramago1715/git_project/-/blob/main/git2.png)

# Apartado gitlab
### Para subirlo tenemos que cambiar el url de origen
![gitlab](https://gitlab.com/Ramago1715/git_project/-/blob/main/gitlab.png)
